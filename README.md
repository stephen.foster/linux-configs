# My i3 Linux config files

these were my config files for **Manjaro-BSPWM**,
I am using the default **Manjaro-BSPWM** config as a baseline, I will be changing them as I learn bspc syntax, but as of now, I have made minor changes to autostart and sxkbd.

I hope you enjoy using them as much as I enjoyed making them!

## Dependancies

1.  i3status
    * py3status
    * FontAwesome (for icons)
    * SauseCodePro font
1.  zsh
    * oh-my-zsh
1.  nvim
    * vim-pathogen
1. bspwm autostart
    * mailsrping
    * numlockx
    * mate-notification-daemon
    * gnome-keyring-daemon
    * polkit-mate-authentication-daemon
    * update-checker
    * light-locker
    * compton
    * fehbg
      - feh
