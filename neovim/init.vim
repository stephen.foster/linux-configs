" All system-wide defaults are set in $VIMRUNTIME/archlinux.vim (usually just
" /usr/share/vim/vimfiles/archlinux.vim) and sourced by the call to :runtime
" you can find below.  If you wish to change any of those settings, you should
" do it in this file (/etc/vimrc), since archlinux.vim will be overwritten
" everytime an upgrade of the vim packages is performed.  It is recommended to
" make changes after sourcing archlinux.vim since it alters the value of the
" 'compatible' option.

" This line should not be removed as it ensures that various options are
" properly set to work with the Vim-related packages.
runtime! archlinux.vim

" ================================================================================
" 				CUSTOM
" ================================================================================
set exrc
set secure
set tabstop=4 softtabstop=4 shiftwidth=4 expandtab autoindent
set cursorline
setlocal spell spelllang=en_gb
set ruler
setl number
set colorcolumn=110
" mouse controls
set mouse=a
set mousef

highlight ColorColumn ctermbg=darkgrey

augroup project
	autocmd!
	autocmd BufRead, BufNewFile *.h, *.c set filetype=c
augroup END

autocmd BufRead *.md set ft=markdown
syntax enable

colorscheme murphy

execute pathogen#infect()
